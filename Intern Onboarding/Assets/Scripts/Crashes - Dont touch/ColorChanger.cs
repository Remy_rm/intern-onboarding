﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.Diagnostics;

public class ColorChanger : MonoBehaviour
{
    Image img;
    int dice = 0;
    System.Random rand;

    // Start is called before the first frame update
    void Start()
    {
        img = GetComponent<Image>();
        rand = new System.Random();
        StartCoroutine(SwitchColour());
    }

    private IEnumerator SwitchColour()
    {
        dice = rand.Next(0, 7);
        switch (dice)
        {
            case 0:
                img.color = Color.white;
                break;

            case 1:
                img.color = Color.red;
                break;

            case 2:
                img.color = Color.blue;
                break;

            case 3:
                img.color = Color.magenta;
                break;
            case 4:
                img.color = Color.yellow;
                break;

            case 5:
                img.color = Color.green;
                break;

            case 6:
                img.color = Color.cyan;
                break;

            default:
                break;
        }

        yield return new WaitForSeconds(1);
        StartCoroutine(SwitchColour());
    }

    public void ButtonClick()
    {
        if (img.color == Color.green || img.color == Color.magenta)
        {
            Debug.LogErrorFormat("Crash! {0}", "");
            UnityEditor.EditorApplication.isPlaying = false;
        }
    }
}
