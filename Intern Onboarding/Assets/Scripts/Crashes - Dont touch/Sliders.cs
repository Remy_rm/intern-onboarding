﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class Sliders : MonoBehaviour
{
    public Slider slider1;
    public Slider slider2;
    public Slider slider3;

    private void Update()
    {
        if (slider1.value > 0.9f && slider2.value < 0.5f && slider3.value > 0.9f)
        {
            Debug.LogErrorFormat("Crash! {0}", "");
            UnityEditor.EditorApplication.isPlaying = false;
        }
    }
}
