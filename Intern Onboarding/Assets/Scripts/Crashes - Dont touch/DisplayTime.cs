﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class DisplayTime : MonoBehaviour
{
    public bool disable;

    private Text txt;
    internal static float time;
    internal static float maxTime;

    // Start is called before the first frame update
    void Start()
    {
        System.Random r = new System.Random();
        maxTime = r.Next(60, 300);
        txt = GetComponent<Text>();
    }

    private void Update()
    {
        time = Time.realtimeSinceStartup;
        var minutes = Mathf.Floor(time / 60);
        var seconds = time % 60;
        txt.text = string.Format("{0}:{1}", minutes.ToString("00"), seconds.ToString("00"));

        if (time > maxTime && !disable)
        {
            UnityEditor.EditorApplication.isPlaying = false;
            Debug.LogErrorFormat("Crash! {0}", "");
        }
    }
}
