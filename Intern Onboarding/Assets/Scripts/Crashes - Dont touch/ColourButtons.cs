﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using System.Linq;
using System;

public class ColourButtons : MonoBehaviour
{
    private int totalClicks = 0;
    private Image img;
    List<int> clickOrder = new List<int>();
    int[] checkOrder = new int[3] { 1, 2, 1 };
    enum Colours { Green, Red, Blue, Magenta, Cyan };
    Colours currentColor;

    private void Start()
    {
        img = GetComponent<Image>();
    }

    public void SwitchColour(int i)
    {
        totalClicks++;

        if (totalClicks > 15)
        {
            UnityEditor.EditorApplication.isPlaying = false;
            Debug.LogErrorFormat("Crash! Last colour: {0}", currentColor);
        }

        switch (i)
        {
            case 0:
                img.color = Color.green;
                break;

            case 1:
                img.color = Color.red;
                break;

            case 2:
                img.color = Color.blue;
                break;

            case 3:
                img.color = Color.magenta;
                break;
            case 4:
                img.color = Color.cyan;
                break;

            default:
                break;
        }
        currentColor = (Colours)i;

        clickOrder.Add(i);

        if (clickOrder.Count > 2)
        {
            int[] lastThree = clickOrder.Skip(clickOrder.Count - 3).Take(3).ToArray();
            if (Enumerable.SequenceEqual(lastThree, checkOrder))
            {
                Debug.LogErrorFormat("Crash! Last colour: {0}", currentColor);
                UnityEditor.EditorApplication.isPlaying = false;
            }
        }
    }
}
