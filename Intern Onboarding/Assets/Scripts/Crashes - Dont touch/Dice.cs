﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class Dice : MonoBehaviour
{
    public Text roll;
    System.Random rand;
    bool rolling = false;
    int lastRoll = -1;

    private void Start()
    {
        rand = new System.Random();
    }

    public void RollDice()
    {
        if (!rolling)
            StartCoroutine(Roll());
    }

    private IEnumerator Roll()
    {
        rolling = true;

        var time = Time.realtimeSinceStartup;

        while (Time.realtimeSinceStartup - time < 0.25f)
        {
            roll.text = rand.Next(0, 7).ToString();
            yield return new WaitForEndOfFrame();
        }

        var thisRoll = rand.Next(0, 7);

        if (lastRoll == thisRoll)
        {
            Debug.LogErrorFormat("Crash! Rolled {0}", thisRoll);
            UnityEditor.EditorApplication.isPlaying = false;
        }
        roll.text = thisRoll.ToString();
        lastRoll = thisRoll;

        rolling = false;
    }
}
