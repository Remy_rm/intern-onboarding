﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Button4 : MonoBehaviour
{
    public void OnClick()
    {
        if (Mathf.RoundToInt(Time.realtimeSinceStartup) % 3 == 0 || Mathf.RoundToInt(Time.realtimeSinceStartup) % 5 == 0)
        {
            Debug.LogErrorFormat("Crash! {0}", "");
            UnityEditor.EditorApplication.isPlaying = false;
        }
    }
}
