﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class FlipSign : MonoBehaviour
{
    [Header("Assignment 1")]
    public int exerciseOne = -10;
    public Text exerciseOneAnswer;

    [Header("Assignment 2")]
    public int exerciseTwo = 5;
    public Text exerciseTwoAnswer;

    [Header("Assignment 3")]
    public int exerciseThree = -10;
    public Text exerciseThreeAnswer;

    void Start()
    {
        //Use Unity's math library to flip the sign (make negative positive)
        exerciseOneAnswer.text = string.Format("1) {0}, Expected result: 10", exerciseOne);

        //Use multiplication to flip the sign
        exerciseTwoAnswer.text = string.Format("2) {0}, Expected result: -5", exerciseTwo);

        //Flip the sign using a bitwise operator
        exerciseThreeAnswer.text = string.Format("3) {0}, Expected result: -42", exerciseThree);
    }
}
