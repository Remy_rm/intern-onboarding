﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class Normalization : MonoBehaviour
{
    public Text currentNormalizedValue;

    public Text currentNormalizedValueStartingAtNonZero;

    public Text normalizedValueInverted;

    public void UpdateNormalizedValue()
    {
        float normalizedValue = -1;

        currentNormalizedValue.text = normalizedValue.ToString("0.00");

        float normalizedValueNonZeroStart = -1;
        currentNormalizedValueStartingAtNonZero.text = normalizedValueNonZeroStart.ToString("0.00");

        float noramlizedValueInversed = -1;
        normalizedValueInverted.text = noramlizedValueInversed.ToString("0.00");
    }
}
