﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MinimizeNewKeyword : MonoBehaviour
{
    public GameObject cube;
    public GameObject sphere;

    private readonly WaitForEndOfFrame waitFrame = new WaitForEndOfFrame();

    public void Start()
    {
        //Place this cube at the position (0, 5, 0) without using the 'new' keyword
        //cube.transform.position = new Vector3(0, 5, 0);
        cube.transform.position = Vector3.up * 5;
        StartCoroutine(DelayPlaceSphere());

    }

    private IEnumerator DelayPlaceSphere()
    {
        //Optimise it so there is no "new WaitForEndOfFrame" allocated every loop, while still waiting 10 frames.
        for (int i = 0; i < 10; i++)
        {
            yield return waitFrame;
            //yield return new WaitForEndOfFrame();
        }
        sphere.transform.position = new Vector3(-5, 5, 0);
    }
}
