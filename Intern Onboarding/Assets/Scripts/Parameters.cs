﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Parameters : MonoBehaviour
{
    private void Start()
    {
        Debug.LogFormat("Random: {0}", GetRandom());
        Debug.LogFormat("Random with seed: {0}", GetRandom(1543));

        int numberNoReturnValue = 0;
        GetRandomWithoutReturnValue(ref numberNoReturnValue);
        Debug.LogFormat("ref: {0}", numberNoReturnValue);
    }
    private int GetRandom()
    {
        System.Random rand = new System.Random();
        return rand.Next(0, 10000);
    }

    private int GetRandom(int seed)
    {
        System.Random rand = new System.Random(seed);
        return rand.Next(0, 10000);
    }

    private int GetRandomWithBounds(int min = 10, int max = 10000)
    {
        System.Random rand = new System.Random();
        return rand.Next(min, max);
    }

    private void GetRandomWithoutReturnValue(ref int randomNumber)
    {
        System.Random rand = new System.Random();
        randomNumber = rand.Next(0, 10000);
    }
}
