﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PersonConstructor : MonoBehaviour
{
    private string name;
    private int age;
    private string occupation;

    public PersonConstructor(string name, int age, string occupation)
    {
        this.name = name;
        this.age = age;
        this.occupation = occupation;
    }
}
