﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;


public class Coroutines : MonoBehaviour
{
    public Image fadeImage;
    public AnimationCurve curve;

    // Start is called before the first frame update
    void Start()
    {
        StartCoroutine(ImWaiting());
    }

    private void Update()
    {
        if (Input.GetKeyDown(KeyCode.O))
        {
            StartCoroutine(Fade(false));
        }
        if (Input.GetKeyDown(KeyCode.P))
        {
            StartCoroutine(Fade(true));
        }
    }

    private IEnumerator Fade(bool fadeIn)
    {
        Color current = fadeImage.color;
        var direction = fadeIn ? 1 : -1;

        float t = current.a;
        var e = curve.Evaluate(t);
        while (t >= 0 && t <= 1)
        {
            t += (0.03f * direction);
            e = curve.Evaluate(t);

            current.a = e;
            fadeImage.color = current;
            yield return new WaitForEndOfFrame();
        }
        current.a = Mathf.Clamp01(current.a);
        fadeImage.color = current;
    }

    private IEnumerator ImWaiting()
    {
        yield return new WaitForEndOfFrame(); // This line is just here to prevent a compiler error, feel free to ignore it!
        Debug.LogFormat("Waiting");
        yield return WaitForMe();
        Debug.LogFormat("Waiting complete");
    }

    private IEnumerator WaitForMe()
    {
        Debug.LogFormat("time to wait 3 seconds");
        yield return new WaitForSeconds(3);
        Debug.LogFormat("3 second wait is over");
    }
}
