﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System.Linq;

public class LINQ : MonoBehaviour
{
    public List<Person> people = new List<Person>();

    // Start is called before the first frame update
    void Start()
    {
        //Print the name of every Person whose name starts with an 'A'
        var peopleStartingWithA = people.FindAll(o => o.name.Substring(0, 1) == "A");
        foreach (var item in peopleStartingWithA)
        {
            Debug.LogFormat("Name: {0}", item.name);
        }

        Debug.LogFormat("####################");

        //Print the age of every person, starting at the youngest age
        var peopleOrderedByAge = people.OrderBy(o => o.age).ToList();
        foreach (var item in peopleOrderedByAge)
        {
            Debug.LogFormat("Age: {0}", item.age);
        }

        Debug.LogFormat("####################");

        peopleOrderedByAge.Reverse(0, peopleOrderedByAge.Count);
        foreach (var item in peopleOrderedByAge)
        {
            Debug.LogFormat("Age: {0}", item.age);
        }
    }
}

[System.Serializable]
public class Person
{
    public string name;
    public int age;
}