﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System.Linq;
using System;

public class AnagramChecker : MonoBehaviour
{
    public string baseWord = "anagram";
    public string[] checkWords = { "nagraam", "bnagram", "maaagrn", "nagnaar" };

    // Start is called before the first frame update
    void Start()
    {
        foreach (var item in checkWords)
        {
            Debug.LogFormat("IsAnagram: {0}", IsAnagram(baseWord, item));
        }
    }

    private bool IsAnagram(string baseWord, string checkWord)
    {
        var baseArray = baseWord.ToCharArray();
        var checkArray = checkWord.ToCharArray();

        Array.Sort(baseArray);
        Array.Sort(checkArray);

        return Enumerable.SequenceEqual(baseArray, checkArray);
    }

}
